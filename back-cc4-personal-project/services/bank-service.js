const bankRepository = require('../repositories/bank-repository');

module.exports = (db) => {
  return {
    findBanks: async () => {
      try {
        const result = await bankRepository(db).findBanks();
        if (result.length === 0) {
          return { httpStatus: 204, message: result };
        } else {
          return { httpStatus: 200, message: result };
        }
      } catch (ex) {
        if (ex.message.includes('ECONNREFUSED')) {
          return { httpStatus: 500, errorMessage: 'Database server error' }
        }
        return { httpStatus: 400, errorMessage: ex };
      }
    },

    findBankByName: async (bankName) => {
      try {
        const result = await bankRepository(db).findBankByName(bankName);
        if (!result) {
          return { httpStatus: 204, message: result };
        } else {
          return { httpStatus: 200, message: result };
        }
      } catch (ex) {
        if (ex.message.includes('ECONNREFUSED')) {
          return { httpStatus: 500, errorMessage: 'Database server error' };
        }
        return { httpStatus: 400, errorMessage: ex };
      }
    },

    findBankById: async (bankId) => {
      try {
        const result = await bankRepository(db).findBankById(bankId);
        if (!result) {
          return { httpStatus: 204, message: result };
        } else {
          return { httpStatus: 200, message: result };
        }
      } catch (ex) {
        if (ex.message.includes('ECONNREFUSED')) {
          return { httpStatus: 500, errorMessage: 'Database server error' };
        }
        return { httpStatus: 400, errorMessage: ex };
      }
    }
  }
}
;
