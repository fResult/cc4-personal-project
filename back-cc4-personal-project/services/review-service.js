const reviewRepository = require('../repositories/review-repository');

module.exports = (db) => {
  return {

    findReviewsByStudentIdAndCourseId: (studentId, courseId) => {
      return reviewRepository(db).findReviewsByStudentIdAndCourseId(studentId, courseId);
    }

  }
};
