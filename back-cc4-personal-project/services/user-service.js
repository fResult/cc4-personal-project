const passport = require('passport');
const jwt = require('jsonwebtoken');
const jwtOptions = require('../config/passport/passport');

const userRepository = require('../repositories/user-repository');

module.exports = (db) => {
  const repository = userRepository(db);

  return {

    findUserByUsername: (username) => {
      return repository.findUserByUsername(username)
    },

    register: (req, userData) => {
      passport.authenticate('local-hashPassword', {}, (err, hashedPassword) => {
        err ? console.error(err) : null;
        const user = { ...userData, password: hashedPassword };

        switch (user.role) {
          case 'TEACHER':
            return repository.createTeacher(user);
          case 'STUDENT':
            return repository.createStudent(user);
          default:
            return '';
        }
      })(req);
    },

    signIn: (req, res, next) => {
      const result = { httpStatus: 500, message: undefined, errorMessage: undefined };
      return new Promise((resolve, reject) => {
        passport.authenticate('local-comparePassword', {}, (err, user, info) => {
          if (err) {
            console.error(err);
          }
          if (info !== undefined) {
            console.error(`Info Message Error: ${info.message}`);
            if (info.message === 'Database server error') {
              result.httpStatus = 500;
              result.message = info.message;
            } else if (!user) {
              result.httpStatus = 401;
              result.message = info.message;
            } else {
              result.httpStatus = 400;
              result.errorMessage = info.message;
            }
            console.log(info.message);
          } else {
            const token = jwt.sign(
              { id: user.id, role: user.role, first_name: user.first_name, last_name: user.last_name },
              jwtOptions.secretOrKey,
              { expiresIn: 3600 }
            );

            result.httpStatus = 200;
            result.message = {
              auth: true,
              token,
              message: 'user found & logged in'
            };
          }
          resolve(result);
        })(req, res, next)
      });
    },

    findTeachersByFirstNameOrLastName: async (firstName, lastName) => {
      const result = await repository.findTeachersByFirstNameOrLastName(firstName, lastName);
      try {
        if (result.length === 0) {
          return { httpStatus: 204, message: result };
        } else {
          return { httpStatus: 200, message: result };
        }
      } catch (ex) {
        if (ex.message.includes('ECONNREFUSED')) {
          return { httpStatus: 500, errorMessage: 'Database server error' }
        }
        return { httpStatus: 400, errorMessage: ex };
      }
    },

    findUsersByIds: async (ids) => {
      const result = await repository.findUsersByIds(ids);
      try {
        if (result.length === 0) {
          return { httpStatus: 204, message: result };
        } else {
          return { httpStatus: 200, message: result };
        }
      } catch (ex) {
        if (ex.message.includes('ECONNREFUSED')) {
          return { httpStatus: 500, errorMessage: 'Database server error' }
        }
        return { httpStatus: 400, errorMessage: ex };
      }
    },

    findStudentByStudentId: async (studentId) => {
      const result = await repository.findStudentByStudentId(studentId);
      try {
        if (!result) {
          return { httpStatus: 204, message: result };
        } else {
          return { httpStatus: 200, message: result };
        }
      } catch (ex) {
        if (ex.message.includes('ECONNREFUSED')) {
          return { httpStatus: 500, errorMessage: 'Database server error' }
        }
        return { httpStatus: 400, errorMessage: ex };
      }
    }
  }
};
