const bankAccountRepository = require('../repositories/bank-account-repository');

module.exports = (db) => {
  return {
    createBankAccount: async (bankAccount) => {
      return await bankAccountRepository(db).createBankAccount(bankAccount);
    },

    findBankAccountByAccountNo: async (accountNo) => {
      return await bankAccountRepository(db).findBankAccountByAccountNo(accountNo);
    },
  }
};
