const courseRepository = require('../repositories/course-repository');

const courseService = (db) => {
  const repository = courseRepository(db);
  return {
    findCourses: async () => {
      try {
        const result = await repository.findCourses();
        if (result.length === 0) {
          return { httpStatus: 204, message: result };
        } else {
          return { httpStatus: 200, message: result };
        }
      } catch (ex) {
        return { httpStatus: 400, errorMessage: ex };
      }
    },

    findCourseById: async (courseId) => {
      try {
        const result = await repository.findCourseById(courseId);
        if (!result) {
          return { httpStatus: 204, message: result };
        } else {
          return { httpStatus: 200, message: result };
        }
      } catch (ex) {
        return { httpStatus: 400, errorMessage: ex.message };
      }
    },

    findCoursesByName: async (courseName) => {
      try {
        const result = await repository.findCoursesByName(courseName);

        if (!result) {
          return { httpStatus: 204, message: result };
        } else {
          return { httpStatus: 200, message: result };
        }
      } catch (ex) {
        return { httpStatus: 400, errorMessage: ex };
      }
    },

    findCoursesByIds: async (ids) => {
      const result = await courseService(db).findCoursesByIds(ids)
      try {
        if (result.length === 0) {
          return { httpStatus: 204, message: result };
        } else {
          return { httpStatus: 200, message: result };
        }
      } catch (ex) {
        if (ex.message.includes('ECONNREFUSED')) {
          return { httpStatus: 500, errorMessage: 'Database server error' }
        }
        return { httpStatus: 400, errorMessage: ex };
      }
    }

  }
};

module.exports = courseService;
