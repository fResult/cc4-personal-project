const courseService = require('../services/course-service');
const userService = require('../services/user-service');

const teachingRepository = require('../repositories/teaching-repository');

module.exports = (db) => {
  return {

    findTeachingByCourseId: (courseId) => {
      return teachingRepository(db).findTeachingByCourseId(courseId);
    },

    findTeachingByCourseNameOrTeacherName: async (courseName, firstName, lastName) => {
      try {
        const targetCourses = (await courseService(db).findCoursesByName(courseName)).message;
        const targetTeachers = (await userService(db).findTeachersByFirstNameOrLastName(firstName, lastName)).message;
        const targetCoursesId = targetCourses.map(targetCourse => targetCourse.id);
        const targetTeachersId = targetTeachers.map(targetTeacher => targetTeacher.id);

        const teachings = await teachingRepository(db).findTeachingByCourseIdOrTeacherId(targetCoursesId, targetTeachersId);

        const courses = [...teachings.map(teaching => teaching.Courses[0])];
        const result = courses.filter((course, idx, a) => a.findIndex(t => (JSON.stringify(t) === JSON.stringify(course))) === idx);

        if (result.length === 0) {
          return { httpStatus: 204, message: result };
        } else {
          return { httpStatus: 200, message: result };
        }
      } catch (ex) {
        if (ex.message.includes('ECONNREFUSED')) {
          return { httpStatus: 500, errorMessage: 'Database server error' }
        }
        return { httpStatus: 400, errorMessage: ex };
      }
    }

  }
};
