const enrollmentRepository = require('../repositories/enrollment-repository');

module.exports = (db) => {
  const repository = enrollmentRepository(db);
  return {

    findEnrollmentsByStudentId: async (studentId) => {
      try {
        const result = await repository.findEnrollmentsByStudentId(studentId);
        if (!result) {
          return { httpStatus: 204, message: result };
        } else {
          return { httpStatus: 200, message: result };
        }
      } catch (ex) {
        if (ex.message.includes('ECONNREFUSED')) {
          return { httpStatus: 500, errorMessage: 'Database server error' };
        }
        return { httpStatus: 400, errorMessage: ex };
      }
    },

    createEnrollment: async (studentId, courseId) => {
      try {
        const targetEnrollment = await repository.findEnrollmentByStudentIdAndCourseId(studentId, courseId);

        if (targetEnrollment) {
          return { httpStatus: 409, errorMessage: 'You have already enroll this course' }
        }

        const result = await repository.createEnrollment(studentId, courseId);
        if (!result) {
          return { httpStatus: 204, message: result };
        } else {
          return { httpStatus: 201, message: result };
        }
      } catch (ex) {
        if (ex.message.includes('ECONNREFUSED')) {
          return { httpStatus: 500, errorMessage: 'Database server error' };
        }
        return { httpStatus: 400, errorMessage: ex };
      }
    }
  }
};
