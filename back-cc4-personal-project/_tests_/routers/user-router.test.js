const request = require('supertest');
const userRouter = require('../../routers/user-router');
const server = require('../../index');
const db = require('../../models');

// prepare User Router
userRouter(server, db);

describe('TEST User Router endpoints', () => {

  it('should create a new user', async () => {
    const res = await request(server)
        .post('/users')
        .send({
          username: "Yoyo",
          password: "Hiya",
          fullName: "Sila Setth",
          role: "TEACHER",
          email: "sila@setth.com",
          bankAccount: {
            accountNo: "1512959642",
            name: "Kapao Ping",
            bankId: 1
          }
        });
    expect(res.statusCode).toEqual(201);
    expect(res.body).toHaveProperty('role');
  })
});
