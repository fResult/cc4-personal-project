const teachingController = require('../controllers/teaching-controller');
const teachingService = require('../services/teaching-service');

module.exports = (server, db) => {

  server.get('/teachings/courses/:courseId', async (req, res) => {
    try {
      res.json(await teachingService(db).findTeachingByCourseId(req.params.courseId));
    } catch (ex) {
      res.status(400).json({errorMessage: ex.message});
    }
  });

  server.get('/teachings', teachingController(db).findTeachingByCourseNameOrTeacherName);

};
