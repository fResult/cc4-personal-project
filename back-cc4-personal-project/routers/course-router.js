const courseController = require('../controllers/course-controller');
const courseService = require('../services/course-service.js');

module.exports = (server, db) => {
  server.get('/courses', courseController(db).findCourses);

  server.get('/courses/:id', courseController(db).findCourseById);
};
