const enrollmentController = require('../controllers/enrollment-controller');

module.exports = (server, db) => {
  const controller = enrollmentController(db);

  server.get('/enrollments/users/:studentId', controller.findEnrollmentByStudentId);

  server.post('/enrollments/users/:studentId/courses/:courseId', controller.createEnrollment);

};
