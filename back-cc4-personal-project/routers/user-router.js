const userController = require('../controllers/user-controller');
const userService = require('../services/user-service');
const bankService = require('../services/bank-service');
const bankAccountService = require('../services/bank-account-service');

module.exports = (server, db) => {
  const controller = userController(db);
  // @TODO: Refactor this router => split another model to its service
  server.post('/users/register', async (req, res) => {
    if (req.body.role === 'TEACHER') {
      const targetBank = (await bankService(db).findBankByName(req.body.bankAccount.bankName)).message/*.dataValues;*/
      if (!targetBank) {
        res.status(404).json({ errorMessage: 'Bank not found' });
      } else {
        try {
          const targetBankAccount = await bankAccountService(db).findBankAccountByAccountNo(req.body.bankAccount.accountNo);
          let user = { ...req.body, bankAccount: { ...req.body.bankAccount, bankId: targetBank.id } };
          if (!targetBankAccount) {
            const newBankAccount = await bankAccountService(db).createBankAccount(user.bankAccount);
            user = { ...user, bankAccount: { ...user.bankAccount, bankAccountId: newBankAccount.id } };
          } else {
            user = { ...user, bankAccount: { ...user.bankAccount, bankAccountId: targetBankAccount.id } };
          }
          try {
            const targetUser = await userService(db).findUserByUsername(user.username);
            if (targetUser !== null) {
              console.info('Username already taken');
              res.status(409).json({ errorMessage: `Username '${targetUser.username}' already taken` });
            } else {
              try {
                await userService(db).register(req, user);
                res.status(201).json({ message: `User '${user.username}' is created` });
              } catch (ex) {
                console.info(`400 createUser: ${ex}`);
                res.status(400).json({ errorMessage: ex });
              }
            }
          } catch (ex) {
            console.info(`400 findOneUser: ${ex}`);
            res.status(400).json({ errorMessage: ex });
          }
        } catch (ex) {
          console.info(`400 createBankAccount: ${ex}`);
          res.status(400).json({ errorMessage: ex.message });
        }
      }
    } else {
      try {
        const targetUser = await userService(db).findUserByUsername(req.body.username);
        if (targetUser !== null) {
          console.info('Username already taken');
          res.status(409).json({ errorMessage: `Username '${targetUser.username}' already taken` });
        } else {
          try {
            await userService(db).register(req, req.body);
            res.status(201).json({ message: `User '${req.body.username}' is created` });
          } catch (ex) {
            console.info(`400 createUser: ${ex}`);
            res.status(400).json({ errorMessage: ex });
          }
        }
      } catch (ex) {
        console.info(`400 findOneUser: ${ex}`);
        res.status(400).json({ errorMessage: ex });
      }
    }

  });

  server.post('/users/sign-in', async (req, res, next) => {
    try {
      await controller.signIn(req, res, next);
    } catch (ex) {
      res.status(400).json({ errorMessage: ex.message });
    }
  })

  server.get('/users', controller.findTeachersByFirstNameOrLastName);

  server.get('/users/:studentId', controller.findStudentByStudentId);

// @TODO: Make Update User

// @TODO: Make Delete User

};
