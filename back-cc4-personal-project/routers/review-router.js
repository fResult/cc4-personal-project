const reviewService = require('../services/review-service');

module.exports = (server, db) => {

  server.get('/reviews/users/:studentId/courses/:courseId', async (req, res) => {
    const {studentId, courseId} = req.params;
    try {
      res.json(await reviewService(db).findReviewsByStudentIdAndCourseId(studentId, courseId));
    } catch(ex) {
      res.status(400).json({errorMessage: ex.message});
    }
  });

};
