const bankController = require('../controllers/bank-controller');

module.exports = (server, db) => {
  server.get('/banks', bankController(db).findBanks);

  server.get('/banks/:name', bankController(db).findBankByBankName);

  server.get('/banks/:id', bankController(db).findBankById);
};
