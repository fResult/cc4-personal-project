'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('courses', [
      {
        name: 'Angular Course',
        desc_short: 'Learn Angular from zero to hero',
        desc_long: 'Learn Angular from zero to hero',
        type: 'ONLINE_COURSE',
        price: 389,
        image_url: 'https://www.pngfind.com/pngs/m/150-1507335_google-has-been-hard-at-work-on-angular.png',
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        name: 'React Course',
        desc_short: 'How to become to React Developer',
        desc_long: 'How to become to React Developer',
        type: 'ONLINE_COURSE',
        price: 389,
        image_url: 'https://braziljs.org/wp-content/uploads/2018/02/react-debora.png',
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        name: 'Vue Course',
        desc_short: 'Vue is simple and easy...',
        desc_long: 'Vue is simple and easy...',
        type: 'ONLINE_COURSE',
        price: 389,
        image_url: 'http://www.somkiat.cc/wp-content/uploads/2017/11/vue.jpg',
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        name: 'Spring boot Course',
        desc_short: 'Spring boot will be the best',
        desc_long: 'Spring boot will be the best',
        type: 'WORKSHOP',
        price: 649,
        image_url: 'https://miro.medium.com/max/600/0*y4bklsYrfPIGCFUA.png',
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        name: 'Golang Course',
        desc_short: 'Develop Web Application with TDD',
        desc_long: 'Develop Web Application with TDD',
        type: 'WORKSHOP',
        price: 649,
        image_url: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTzkmyYcHgcUGhEfSanGsLKwPoV8miAsqqnmaWZOMyvVCkHDt3E&s',
        created_at: new Date(),
        updated_at: new Date(),
      },
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
  }
};
