'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('teachings', [
      {
        teacher_id: 1,
        course_id: 1,
        started_date: new Date(),
        ended_date: new Date(),
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        teacher_id: 2,
        course_id: 1,
        started_date: new Date(),
        ended_date: new Date(),
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        teacher_id: 1,
        course_id: 2,
        started_date: new Date(),
        ended_date: new Date(),
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        teacher_id: 2,
        course_id: 3,
        started_date: new Date(),
        ended_date: new Date(),
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        teacher_id: 1,
        course_id: 4,
        started_date: new Date(),
        ended_date: new Date(),
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        teacher_id: 2,
        course_id: 4,
        started_date: new Date(),
        ended_date: new Date(),
        created_at: new Date(),
        updated_at: new Date(),
      }
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
  }
};
