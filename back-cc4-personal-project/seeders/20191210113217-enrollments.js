'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('enrollments', [
      {
        student_id: 4,
        course_id: 4,
        enrolled_date: new Date(),
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        student_id: 1,
        course_id: 5,
        enrolled_date: new Date(),
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        student_id: 3,
        course_id: 2,
        enrolled_date: new Date(),
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        student_id: 4,
        course_id: 3,
        enrolled_date: new Date(),
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        student_id: 2,
        course_id: 5,
        enrolled_date: new Date(),
        created_at: new Date(),
        updated_at: new Date(),
      },
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
  }
};
