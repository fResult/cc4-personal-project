'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {

    return queryInterface.bulkInsert('banks',
        [
          {
            name: 'KBANK',
            name_th: 'กสิกรไทย',
            created_at: new Date(),
            updated_at: new Date(),
          },
          {
            name: 'KTB',
            name_th: 'กรุงไทย',
            created_at: new Date(),
            updated_at: new Date(),
          },
          {
            name: 'TMB',
            name_th: 'ทหารไทย',
            created_at: new Date(),
            updated_at: new Date(),
          },
          {
            name: 'TANACHART',
            name_th: 'ธนชาติ',
            created_at: new Date(),
            updated_at: new Date(),
          },
          {
            name: 'SCB',
            name_th: 'ไทยพาณิชย์',
            created_at: new Date(),
            updated_at: new Date(),
          },
          {
            name: 'UOB',
            name_th: 'ยูโอบี',
            created_at: new Date(),
            updated_at: new Date(),
          },
          {
            name: 'CITY_BANK',
            name_th: 'ซิตี้แบงค์',
            created_at: new Date(),
            updated_at: new Date(),
          },
          {
            name: 'GSB',
            name_th: 'ออมสิน',
            created_at: new Date(),
            updated_at: new Date(),
          },
        ]
        , {}, {}
        )
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
  }
};
