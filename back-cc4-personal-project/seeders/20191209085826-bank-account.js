'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('bank_accounts', [
      {
        account_no: '0618595603',
        name: 'CoderTeam',
        bank_id: 1,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        account_no: '1312208729',
        name: 'HelloWorld',
        bank_id: 3,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        account_no: '0154560918',
        name: 'MatrixReturn',
        bank_id: 5,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        account_no: '0470351121',
        name: 'yaHooTeam',
        bank_id: 2,
        created_at: new Date(),
        updated_at: new Date()
      }
    ], {}, {});
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
  }
};
