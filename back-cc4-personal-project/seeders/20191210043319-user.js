'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('users', [
      {
        username: 'admin',
        password: 'admin',
        first_name: 'Admin',
        last_name: 'Admin',
        role: 'ADMIN',
        email: 'admin@admin.com',
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        username: 'TheOne',
        password: 'TheTwo',
        first_name: 'TheOne',
        last_name: 'Neo',
        role: 'TEACHER',
        email: 'TheOne@TheTwo.dev',
        bank_account_id: 3,
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        username: 'AnanB',
        password: 'AnAnt',
        first_name: 'Anan',
        last_name: 'Boonkla',
        role: 'TEACHER',
        email: 'anan.b@anant.dev',
        bank_account_id: 3,
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        username: 'fResult',
        password: '5555555',
        first_name: 'Sila',
        last_name: 'Setthakan-anan',
        role: 'STUDENT',
        email: 'styxmaz@gmail.com',
        bank_account_id: null,
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        username: 'dislike',
        password: '66666',
        first_name: 'Chaiyo',
        last_name: 'Hohew',
        role: 'STUDENT',
        email: 'chaiyo@hohew.com',
        bank_account_id: null,
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        username: 'ckid',
        password: '7777',
        first_name: 'CoolKids',
        last_name: 'OhoYoyo',
        role: 'STUDENT',
        email: 'coolkids@OhoYoyo.com',
        bank_account_id: null,
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        username: 'aha',
        password: '8888',
        first_name: '',
        last_name: '',
        role: 'STUDENT',
        email: 'abc@abc.com',
        bank_account_id: null,
        created_at: new Date(),
        updated_at: new Date(),
      },
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
  }
};
