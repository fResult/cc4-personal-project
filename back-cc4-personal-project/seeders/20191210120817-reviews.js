'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('reviews', [
      {
        student_id: 5,
        course_id: 3,
        scores: 10,
        content: 'สอนดีมากครับ',
        reviewed_date: new Date(),
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        student_id: 4,
        course_id: 1,
        scores: 3,
        content: 'ห่วยแตก',
        reviewed_date: new Date(),
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        student_id: 1,
        course_id: 5,
        scores: 3,
        content: 'ยังอ่อนหัดอยู่นะ',
        reviewed_date: new Date(),
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        student_id: 5,
        course_id: 5,
        scores: 0,
        content: 'อย่าหวังจะได้กินเงินตรูอีก',
        reviewed_date: new Date(),
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        student_id: 2,
        course_id: 2,
        scores: 8,
        content: 'ทำคอร์สดีๆ ออกมาอีกนะครับ',
        reviewed_date: new Date(),
        created_at: new Date(),
        updated_at: new Date(),
      },
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
  }
};
