'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('chapters', [
      {
        name: 'Introduction to Angular',
        course_id: 1,
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        name: 'Creat project',
        course_id: 1,
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        name: 'Try TypeScript',
        course_id: 1,
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        name: 'Introduction to React',
        course_id: 2,
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        name: 'Functional Programming',
        course_id: 2,
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        name: 'Redux - State management',
        course_id: 2,
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        name: 'Introduction to VueJS',
        course_id: 3,
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        name: 'Vue cli',
        course_id: 3,
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        name: 'VueX - Store management',
        course_id: 3,
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        name: 'Introduction to Spring',
        course_id: 4,
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        name: 'Spring Fundamental',
        course_id: 4,
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        name: 'JPA - Object Relational Management framework',
        course_id: 4,
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        name: 'Introduction to Golang',
        course_id: 5,
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        name: 'Unlearn your OOP',
        course_id: 5,
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        name: 'You are the next Gopher',
        course_id: 5,
        created_at: new Date(),
        updated_at: new Date(),
      },
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('chapters', null, {});
    */
  }
};
