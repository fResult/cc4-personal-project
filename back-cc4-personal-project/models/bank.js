const Sequelize = require('sequelize');

module.exports = (sequelize, DataTypes) => {

  const bank = sequelize.define('bank', {
    name: {
      type: DataTypes.STRING,
    },
    nameTh: {
      type: DataTypes.STRING,
    }
  }, { underscored: true });

  bank.associate = function (models) {
    bank.hasMany(models.bank_account, {foreignKey: {name: 'bank_id', allowNull: false}});
  };

  return bank;
};
