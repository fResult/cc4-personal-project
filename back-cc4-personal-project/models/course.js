const Sequelize = require('sequelize');

const ONLINE_COURSE = 'ONLINE_COURSE';
const WORKSHOP = 'WORKSHOP';

module.exports = (sequelize, DataTypes) => {
  const course = sequelize.define('course', {
    name: {
      type: DataTypes.STRING(140),
      allowNull: false
    },
    desc_short: DataTypes.STRING(140),
    desc_long: {
      type: DataTypes.STRING(1200),
      allowNull: false
    },
    price: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    image_url: {
      type: DataTypes.STRING(1200),
      allowNull: false
    },
    type: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: ONLINE_COURSE,
      validate: {
        isIn: [ONLINE_COURSE, WORKSHOP]
      }
    }
  });

  course.associate = (models) => {
    course.belongsToMany(models.user, {
      through: models.enrollment,
      as: 'user_enrolled',
      foreignKey: {
        name: 'course_id',
        allowNull: false
      }
    });

    course.belongsToMany(models.user, {
      through: models.review,
      as: 'user_reviewed',
      foreignKey: {
        name: 'course_id',
        allowNull: false
      }
    });

    course.belongsToMany(models.user, {
      through: models.teaching,
      as: 'user_taught',
      foreignKey: {
        name: 'course_id',
        allowNull: false
      }
    });

    // @TODO: Move this COURSE API for this into TEACHING API
    course.hasMany(models.chapter, { foreignKey: 'course_id' });
  };

  return course;
};
