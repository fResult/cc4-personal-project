const Sequelize = require('sequelize');

module.exports = (sequelize, DataTypes) => {

  const review = sequelize.define('review', {
    // student_id: {
    //   type: DataTypes.STRING,
    //   references: {
    //     model: 'user',
    //     key: 'id'
    //   }
    // },
    // course_id: {
    //   type: DataTypes.INTEGER,
    //   references: {
    //     model: 'course',
    //     key: 'id'
    //   }
    // },
    scores: {
      type: DataTypes.INTEGER,
      allowNull: false,
      validate: {
        max: 10,
        min: 0,
      }
    },
    content: {
      type: DataTypes.STRING,
    },
    reviewed_date: {
      type: DataTypes.DATE,
      allowNull: false
    }
  });

  // review.associate = (models) => {
  //   review.hasMany(models.user, {foreignKey: 'user_id'});
  //   review.hasMany(models.course, {foreignKey: 'course_id'});
  // };

  return review;
};