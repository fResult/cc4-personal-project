const Sequelize = require('sequelize');

module.exports = (sequelize, DataTypes) => {

  const teaching = sequelize.define('teaching', {
    // teacher_id: {
    //   type: Sequelize.INTEGER,
    //   references: {
    //     model: 'user',
    //     key: 'id'
    //   }
    // },
    // course_id: {
    //   type: Sequelize.INTEGER,
    //   references: {
    //     model: 'course',
    //     key: 'id'
    //   }
    // },
    started_date: {
      type: DataTypes.DATE,
      allowNull: false
    },
    ended_date: {
      type: DataTypes.DATE,
      allowNull: false
    }
  });

  // teaching.associate = (models) => {
  //   teaching.hasMany(models.user, { foreignKey: 'user_id' });
  //   teaching.hasMany(models.course, { foreignKey: 'course_id' });
  // };

  return teaching;
};
