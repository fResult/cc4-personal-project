module.exports = (sequelize, DataTypes) => {

  const enrollment = sequelize.define('enrollment', {
    enrolled_date: {
      type: DataTypes.DATE,
      allowNull: false
    }
  });

  enrollment.associate = (models) => {
    enrollment.hasMany(models.user, { foreignKey: 'user_id' });
    enrollment.hasMany(models.course, { foreignKey: 'course_id' });
  };

  return enrollment;
};
