const Sequelize = require('sequelize');

module.exports = (sequelize, DataTypes) => {

  const chapter = sequelize.define('chapter', {
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    // course_id: {
    //   type: DataTypes.INTEGER,
    //   references: {
    //     model: 'course',
    //     key: 'id'
    //   }
    // }
  }, { timestamp: false });

  chapter.associate = (models) => {
    chapter.belongsTo(models.course, { foreignKey: 'course_id' })
  };

  return chapter;
};
