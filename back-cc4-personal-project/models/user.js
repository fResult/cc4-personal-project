const Sequelize = require('sequelize');

const STUDENT = 'STUDENT';
const TEACHER = 'TEACHER';

module.exports = (sequelize, DataTypes) => {

  const user = sequelize.define('user', {
    username: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
      validate: {
        is: ["^[a-z0-9]+$", 'i'],
      }
    },
    // @TODO Password will be hashed
    password: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        len: 20
      }
    },
    first_name: {
      type: DataTypes.STRING(60),
      allowNull: false,
      validate: {
        is: ["^[a-zA-Z\-]+$", 'i'],
      }
    },
    last_name: {
      type: DataTypes.STRING(60),
      allowNull: false,
      validate: {
        is: ["^[a-zA-Z\-]+$", 'i'],
      }
    },
    role: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        isIn: [[STUDENT, TEACHER]] //ADMIN is only registered in DBMS
      }
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        isEmail: true
      }
    },
    about_teacher: {
      type: DataTypes.STRING(1500)
    },
    img_url: {
      type: DataTypes.STRING(500)
    }
  }, {
    timestamp: false,
    getterMethods: {
      full_name() {
        return `${this.first_name} ${this.last_name}`
      }
    },
    setterMethods: {
      full_name(value) {
        if (value !== undefined || value !== null) {
          const names = value.split(' ');
          this.setDataValue('first_name', names.slice(0, -1).join(' ')); // names[0]
          this.setDataValue('last_name', names.slice(-1).join(' '));     // names[1]
        }
      }
    }
  });

  user.associate = (models) => {
    user.belongsToMany(models.course, {
      through: models.enrollment,
      as: 'course_enrolled',
      foreignKey: {
        name: 'student_id',
        allowNull: false
      }
    });

    user.belongsToMany(models.course, {
      through: models.review,
      as: 'course_reviewed',
      foreignKey: {
        name: 'student_id',
        allowNull: false
      }
    });

    user.belongsToMany(models.course, {
      through: models.teaching,
      as: 'course_taught',
      foreignKey: {
        name: 'teacher_id',
        allowNull: false
      }
    });

    user.belongsTo(models.bank_account, { foreignKey: 'bank_account_id' });
  };

  return user;
};
