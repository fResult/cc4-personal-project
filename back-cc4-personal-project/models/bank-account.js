const Sequelize = require('sequelize');

module.exports = (sequelize, DataTypes) => {

  const bankAccount = sequelize.define('bank_account', {
    account_no: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    name: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    // bank_id: {
    //   type: DataTypes.INTEGER,
    //   reference: {
    //     model: 'bank',
    //     key: 'id'
    //   }
    // },
  }, { timestamp: false });

  bankAccount.associate = (models) => {
    bankAccount.belongsTo(models.bank, {foreignKey: {name: 'bank_id', allowNull: false}});
    bankAccount.hasMany(models.user, {foreignKey: {name: 'bank_account_id', allowNull: true}})
  };

  return bankAccount;
};
