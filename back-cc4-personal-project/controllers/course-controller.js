const courseService = require('../services/course-service');

module.exports = (db) => {
  return {
    findCourses: async (req, res) => {
      try {
        let result;
        if (Object.entries(req.query).length === 0) {
          result = await courseService(db).findCourses();
        } else {
          result = await courseService(db).findCoursesByName(req.query.courseName);
        }

        const { httpStatus, message, errorMessage } = result;
        if (!errorMessage) {
          res.status(httpStatus).json(message);
        } else {
          res.status(httpStatus).json({ errorMessage: errorMessage });
        }
      } catch (ex) {
        res.status(400).json({ errorMessage: ex.message });
      }
    },

    findCourseById: async (req, res) => {
      try {
        const { httpStatus, message, errorMessage } = await courseService(db).findCourseById(req.params.id);
        if (!errorMessage) {
          res.status(httpStatus).json(message);
        } else {
          res.status(httpStatus).json({ errorMessage: errorMessage });
        }
      } catch (ex) {
        res.status(400).json({ errorMessage: ex.message });
      }
    }
  }
};