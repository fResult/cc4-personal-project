const enrollmentService = require('../services/enrollment-service');

module.exports = (db) => {
  const service = enrollmentService(db);

  return {
    findEnrollmentByStudentId: async (req, res) => {
      try {
        const result = await enrollmentService(db).findEnrollmentsByStudentId(req.params.studentId)
        const { httpStatus, message, errorMessage } = result;
        if (!errorMessage) {
          res.status(httpStatus).json(message);
        } else {
          res.status(httpStatus).json({ errorMessage });
        }
      } catch (ex) {
        res.status(400).json({ errorMessage: ex.message });
      }
    },

    createEnrollment: async (req, res) => {
      try {
        const result = await service.createEnrollment(req.params.studentId, req.params.courseId);
        const { httpStatus, message, errorMessage } = result;
        if (!errorMessage) {
          res.status(httpStatus).json(message);
        } else {
          res.status(httpStatus).json({ errorMessage });
        }
      } catch (ex) {
        res.status(400).json({ errorMessage: ex.message });
      }
    }
  }
};
