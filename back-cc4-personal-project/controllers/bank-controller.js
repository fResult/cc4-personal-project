const bankService = require('../services/bank-service');
module.exports = (db) => {
  return {
    findBanks: async (req, res) => {
      try {
        const result = await bankService(db).findBanks();
        if (!result.errorMessage) {
          res.status(result.httpStatus).json(result.message);
        } else {
          res.status(result.httpStatus).json({ errorMessage: result.errorMessage });
        }
      } catch (ex) {
        res.status(400).json({ errorMessage: ex.message });
      }
    },

    findBankByBankName: async (req, res) => {
      try {
        const result = await bankService(db).findBankByName(req.params.name);
        if (!result.errorMessage) {
          res.status(result.httpStatus).json(result.message);
        } else {
          res.status(result.httpStatus).json({ errorMessage: result.errorMessage });
        }
      } catch (ex) {
        res.status(400).json({ errorMessage: ex.message });
      }
    },

    findBankById: async (req, res) => {
      try {
        const result = await bankService(db).findBankById(req.params.id);
        if (!result.errorMessage) {
          res.status(result.httpStatus).json(result.message);
        } else {
          res.status(result.httpStatus).json({ errorMessage: result.errorMessage });
        }
      } catch (ex) {
        res.status(400).json({ errorMessage: ex.message });
      }
    }
  }
};