const teachingService = require('../services/teaching-service');

module.exports = (db) => {
  return {
    findTeachingByCourseNameOrTeacherName: async (req, res) => {
      const { courseName, firstName, lastName } = req.query;
      try {
        const {
          httpStatus,
          message,
          errorMessage
        } = await teachingService(db).findTeachingByCourseNameOrTeacherName(courseName, firstName, lastName);
        if (!errorMessage) {
          res.status(httpStatus).json(message);
        } else {
          res.status(httpStatus).json({ errorMessage: errorMessage });
        }

      } catch (ex) {
        res.status(400).json({ errorMessage: ex.message });
      }
    }
  }
};
