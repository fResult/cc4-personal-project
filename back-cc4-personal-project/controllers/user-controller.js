const userService = require('../services/user-service');

module.exports = (db) => {
  const service = userService(db);

  return {
    signIn: async (req, res, next) => {
      try {
        const result = await service.signIn(req, res, next);
        if (result.httpStatus === 200) {
          res.status(result.httpStatus).json(result.message);
        } else {
          res.status(result.httpStatus).json({ errorMessage: result.message });
        }
      } catch (ex) {
        console.log(ex.message);
        res.status(400).json({ errorMessage: ex.message })
      }
    },

    findTeachersByFirstNameOrLastName: async (req, res) => {
      const { firstName, lastName } = req.query;
      try {
        const {httpStatus, message, errorMessage} = await service.findTeachersByFirstNameOrLastName(firstName, lastName);
        if (!errorMessage) {
          res.status(httpStatus).json(message);
        } else {
          res.status(httpStatus).json({ errorMessage: errorMessage });
        }
      } catch (ex) {
        res.status(400).json({ errorMessage: ex.message })
      }
    },

    findStudentByStudentId: async (req,res) => {
      try {
        const {httpStatus, message, errorMessage} = await service.findStudentByStudentId(req.params.studentId);
        if (!errorMessage) {
          res.status(httpStatus).json(message);
        } else {
          res.status(httpStatus).json({ errorMessage: errorMessage });
        }
      } catch (ex) {
        res.status(400).json({ errorMessage: ex.message })
      }
    }
  }
};