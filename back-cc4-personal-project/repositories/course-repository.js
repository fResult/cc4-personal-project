const Sequelize = require('sequelize');
const Op = Sequelize.Op;

module.exports = (db) => {
  return {

    findCourses: () => {
      return db.course.findAll();
    },

    findCourseById: async (courseId) => {
      return db.course.findByPk(courseId, {
        include: [
          {
            as: 'course1',
            model: db.user,
            attributes: ['id', 'first_name', 'last_name', 'role', 'email', 'about_teacher', 'img_url'],
          },
          { model: db.chapter }
        ],
      });

    },

    findCoursesByName: (courseName) => {
      return db.course.findAll({
        where: { name: { [Op.like]: `%${courseName}%` } },
      });
    },

    findCoursesByIds: (ids) => {
      return db.course.findAll({
        where: {
          id: { [Op.in]: ids }
        },
        include: [db.user]
      })
    }

  }
};
