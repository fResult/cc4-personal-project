module.exports = (db) => {
  return {

    findReviewsByStudentIdAndCourseId: (studentId, courseId) => {
      return db.review.findAll({
        // @FIXME courseId key cannot resolve in Review model => so, i used CourseId field now (now only CourseId key was resolved
        where: {student_id: studentId, course_id: courseId}
      });
    }

  }
};
