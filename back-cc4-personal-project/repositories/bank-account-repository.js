module.exports = (db) => {
  return {
    findBankAccountByAccountNo: (accountNo) => {
      return db.bank_account.findOne({
        where: { account_no: accountNo }
      });
    },

    createBankAccount: (bankAccount) => {
      const { accountNo, name, bankId } = bankAccount;
      return db.bank_account.create({ account_no: accountNo, name, bank_id: bankId });
    }
  }
};
