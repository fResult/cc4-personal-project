const Sequelize = require('sequelize');
const Op = Sequelize.Op;

module.exports = (db) => {
  return {

    findUserByUsername: (username) => {
      return db.user.findOne({
        where: { username: username }
      })
    },

    createTeacher: async (teacherData) => {
      const { username, password, fullName, role, email, bankAccount: { bankAccountId } } = teacherData;
      return await db.user.create({
        username, password,
        full_name: fullName,
        role, email,
        bank_account_id: bankAccountId
      });
    },

    createStudent: async (studentData) => {
      const { username, password, fullName, role, email } = studentData;
      return db.user.create({ username, password, full_name: fullName, role, email });
    },

    signIn: ({ username, password }) => {
      return db.user.findAll({
        attributes: ['username', 'password'],
        where: {
          [Op.and]: [{ username }, { password }]
        }
      });
    },

    findTeachersByFirstNameOrLastName: (firstName = null, lastName = null) => {
      return db.user.findAll({
        where: {
          [Op.or]: [
            { first_name: { [Op.like]: `%${firstName}%` } },
            { last_name: { [Op.like]: `%${lastName}%` } }
          ],
          role: 'TEACHER'
        },
        include: [db.course]
      })
    },

    findUsersByIds: (ids) => {
      return db.user.findAll({
        where: {
          id: { [Op.in]: ids }
        },
        raw: true
      })
    },

    findStudentByStudentId: (studentId) => {
      return db.user.findByPk(studentId, {
        attributes: ['first_name', 'last_name', 'role', 'email', 'about_teacher', 'img_url'],
        include: [{
          as: 'course_enrolled',
          model: db.course
        }]
      })
    }
  }
};
