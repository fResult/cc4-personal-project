module.exports = (db) => {
  return {

    findEnrollmentsByStudentId: (studentId) => {
      return db.enrollment.findAll({
        where: { student_id: studentId }
      });
    },

    createEnrollment: (studentId, courseId) => {
      console.log(studentId)
      return db.enrollment.create({ student_id: studentId, courseId: courseId, enrolled_date: new Date() });
    }

  }
};
