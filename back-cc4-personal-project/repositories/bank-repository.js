module.exports = (db) => {
  return {
    findBanks: () => {
      return db.bank.findAll();
    },

    findBankByName: (bankName) => {
      return db.bank.findOne({ where: { name: bankName } });
    },

    findBankById: (bankId) => {
      return db.bank.findOne({ where: { id: bankId } });
    },
  }
};
