const Sequelize = require('sequelize');
const Op = Sequelize.Op;

module.exports = (db) => {
  return {

    findTeachingByCourseId: (courseId) => {
      return db.teaching.findAll({
        where: { course_id: courseId },
        include: [
          db.User,
          { model: db.course }
        ]
      })
    },

    findTeachingByCourseIdOrTeacherId: (coursesId = null, teachersId = null) => {
      return db.teaching.findAll({
        where: {
          [Op.or]: [
            { course_id: { [Op.in]: coursesId } },
            { teacher_id: { [Op.in]: teachersId } }
          ]
        },
        include: [
          { model: db.user, order: [['id', 'desc']], include: [{ model: db.course, order: [['id', 'desc']] }] },
          { model: db.course, order: [['id', 'desc']] }
        ]
      });
    }
  }
};
