const express = require('express');
const server = express();
const bodyParser = require('body-parser');
const cors = require('cors');
const db = require('./models');
const passport = require('passport');

const userRouter = require('./routers/user-router');
const courseRouter = require('./routers/course-router');
const reviewRouter = require('./routers/review-router');
const enrollmentRouter = require('./routers/enrollment-router');
const teachingRouter = require('./routers/teaching-router');
const bankRouter = require('./routers/bank-router');

const PORT = 3333;

server.use(passport.initialize({}));
server.use(cors());
server.use(bodyParser.json());
server.use(bodyParser.urlencoded({ extended: true }));

// import passport config
require('./config/passport/passport');

db.sequelize.sync({ alter: false, force: false }).then(() => {
  courseRouter(server, db);
  enrollmentRouter(server, db);
  reviewRouter(server, db);
  userRouter(server, db);
  teachingRouter(server, db);
  bankRouter(server, db);

  server.listen(PORT, () => console.log("Backend is started with port:", PORT));
});

module.exports = server;
