import React, { Component } from 'react';
import { Row, Col, Form, Input, Button, notification, Icon } from 'antd';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import jwtDecode from 'jwt-decode';

import { signIn } from '../redux/actions/action-creators';
import axios from '../configs/api.service';

class SignIn extends Component {
  state = {
    isDirty: false,
    username: '',
    password: ''
  };

  // @FIXME User and Password is Invalid could show in frontend log
  openLogInSuccessNotification = () => {
    const key = Date.now();
    notification.open({
      message: 'Log in Successfully',
      description: `${this.props.user.firstName} is logged in`,
      key, onClose: '', placement: 'topRight',
      icon: <Icon type="check-circle" theme="filled" style={{ fontSize: 28, color: '#0B8235' }} />
    })
  };

  openLogInFailedNotification = (message) => {
    const key = Date.now();
    notification.open({
      message: 'Log in failed',
      description: message,
      key, onClose: '', placement: 'topRight',
      icon: <Icon type="close-circle" theme="filled" style={{ fontSize: 28, color: 'crimson' }} />
    })
  };

  handleSignIn = (e) => {
    const { form, signIn, history } = this.props;

    e.preventDefault();
    form.validateFieldsAndScroll(async (error, value) => {
      if (!error) {
        try {
          const loggedInResult = await axios.post('users/sign-in', {
            username: value.username,
            password: value.password
          });
          const user = jwtDecode(loggedInResult.data.token);
          signIn(user, loggedInResult.data.token);
          localStorage.setItem('ACCESS_TOKEN', loggedInResult.data.token);
          this.openLogInSuccessNotification();
          history.push('/', { user });
        } catch (ex) {
          this.openLogInFailedNotification(ex.response.data.errorMessage);
          console.error(ex.response.data);
          form.resetFields('password');
        }
      }
    });
  };

  render() {
    const { form } = this.props;
    return (
      <>
        <Row>
          <Col xxl={9} xl={8} lg={7} md={6} sm={5} xs={4} />

          <Col xxl={6} xl={8} lg={10} md={12} sm={14} xs={16}>
            <Form onSubmit={this.handleSignIn}>
              <Form.Item label="Username">{
                form.getFieldDecorator('username', {
                  rules: [{
                    required: true,
                    message: 'Username must not be null'
                  }]
                })(<Input onChange={(e) => this.setState({ username: e.target.value })} />)
              }</Form.Item>
              <Form.Item label="Password">{
                form.getFieldDecorator('password', {
                  rules: [{
                    required: true,
                    message: 'Password must not be null'
                  }]
                })(
                  <Input type="password"
                         onChange={(e) => this.setState({ password: e.target.value })}
                  />
                )
              }</Form.Item>
              <Form.Item>
                <div style={{ textAlign: 'center' }}>
                  <Button type="primary" htmlType="submit" className="login-form-button">
                    Sign in
                  </Button>
                </div>
              </Form.Item>
            </Form>
          </Col>

          <Col xxl={9} xl={8} lg={7} md={6} sm={5} xs={4} />
        </Row>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    user: state.user
  }
};

const mapDispatchToProps = {
  signIn: signIn,
};

const signInForm = Form.create({ name: 'signIn' })(SignIn);
const connectSignInForm = connect(mapStateToProps, mapDispatchToProps)(signInForm);

export default withRouter(connectSignInForm);
