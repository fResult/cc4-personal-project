import React, { Component } from 'react';
import { Layout } from 'antd'
import { Route, Switch } from "react-router-dom";

import axios from '../configs/api.service';
import Courses from '../components/courses/courses';
import Course from '../components/course/course';
import NavBar from '../components/nav-bar/nav-bar';

import styles from './home.module.css';

const { Header, Content, Footer } = Layout;

class Home extends Component {
  state = {
    courses: [],
    course: {},
  };

  componentDidMount = async () => {
    const courses = (await axios.get('/courses')).data;
    this.setState({ courses });
  };

  handleSelectCourse = async (courseId) => {
    const course = (await axios.get(`/courses/${courseId}`)).data;
    this.setState({ course });
    localStorage.setItem('COURSE', JSON.stringify(course));
    this.forceUpdate();
  };

  render() {
    return (
      <>
        <Layout className={styles.Layout} style={{ zIndex: 0 }}>
          <Header style={{ position: 'fixed', width: '100%', height: 70, background: '#4B99F5', zIndex: 10 }}>
            <NavBar />
          </Header>
          <Content className={styles.Content} style={{ padding: '0', marginTop: 134 }}>
            <div style={{ padding: 24, minHeight: 760 }}>
              <Switch>
                <Route exact path="/"
                       component={() => (
                         <Courses courses={this.state.courses}
                                  onSelectCourse={this.handleSelectCourse}
                         />
                       )}
                />
                <Route exact path="/courses/:courseId" component={() => <Course course={this.state.course} />} />
              </Switch>
            </div>
          </Content>
          <Footer style={{ textAlign: 'center' }}>Athena ©2020 Created by fResult</Footer>
        </Layout>
      </>
    );
  }
}

export default Home;
