import React, { Component } from 'react';
import axios from '../configs/api.service';
import { withRouter } from "react-router-dom";
import { Form, Input, Icon, Select, Row, Col, Button, notification } from 'antd';

const { Option } = Select;

// const AutoCompleteOption = AutoComplete.Option;

class SignUp extends Component {
  roles = ['Teacher', 'Student'];
  state = {
    isDirty: false,
    role: '',
    accountNo: '',
    accountName: '',
    email: '',

    bankList: []
  };

  componentDidMount = async () => {
    const banks = (await axios.get('/banks')).data;
    this.setState({ bankList: banks });
  };



  openRegisterFailedNotification = (message) => {
    const key = Date.now();
    notification.open({
      message: 'Sign up failed',
      description: message,
      key, onClose: '', placement: 'topRight',
      icon: <Icon type="close-circle" style={{ fontSize: 28, color: 'crimson' }} />
    });
  };

  handleSelectRole = value => {
    this.setState({ role: value })
  };

  handleSelectBank = value => {
    this.setState({ bank: value });
  };

  handleRegister = (e) => {
    e.preventDefault();
    const { form } = this.props;

    // @FIXME SignUp with validate this form
    form.validateFieldsAndScroll(async (err, values) => {
      if (!err) {
        try {
          const bankAccount = { bankName: values.bank, accountNo: values.accountNo, name: values.accountName };
          await axios.post('http://localhost:3333/users/register', {
            username: values.username,
            password: values.password,
            confirmPassword: values.confirmPassword,
            email: values.email,
            role: values.role,
            fullName: `${values.firstName} ${values.lastName}`,
            bankAccount
          });
          this.props.history.push('/');
        } catch (ex) {
          this.openRegisterFailedNotification(ex.response.data.errorMessage);
          console.error(ex.response.data);
        }
      }
    });
  };


  validateAccNo = (rule, value, callback) => {
    try {
      if (!/^\d{10}$/.test(value)) {
        callback('Account No must only number 10 digits')
      }
      callback()
    } catch (err) {
      callback(err);
    }
  };

  handleConfirmBlur = e => {
    const { value } = e.target;
    this.setState({ isDirty: this.state.isDirty || !!value });
  };

  compareConfirmPasswordWithPassword = (rule, value, callback) => {
    const { form } = this.props;
    if (value && value !== form.getFieldValue('password')) {
      callback('Two passwords that you enter is inconsistent!');
    } else {
      callback();
    }
  };

  comparePasswordWithConfirmPassword = (rule, value, callback) => {
    const { form } = this.props;
    if (value && this.state.isDirty) {
      form.validateFields(['confirm'], { force: true });
    }
    callback();
  };

  render() {
    const { form } = this.props;
    const { role, bankList } = this.state;
    return (
      <>
        <div style={{ textAlign: 'center', width: '100%' }}>
          <Button type="button" style={{ background: 'crimson', height: 200, width: 200 }} />
        </div>
        <Row>
          <Col xxl={9} xl={8} lg={7} md={6} sm={5} xs={4} />

          <Col xxl={6} xl={8} lg={10} md={12} sm={14} xs={16}>
            <Form onSubmit={this.handleRegister} className="login-form"
                  style={{ maxWidth: '400px', width: '100%', margin: 'auto' }}
            >
              <Row>
                <Form.Item label="Username">{
                  form.getFieldDecorator('username', {
                    rules: [{
                      required: true,
                    }]
                  })(<Input
                    prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                    onChange={e => this.setState({ username: e.target.value })}
                  />)
                }</Form.Item>
                <Form.Item label="Password">{
                  form.getFieldDecorator('password', {
                    rules: [
                      { required: true, message: 'Password is required' },
                      { validator: this.comparePasswordWithConfirmPassword }
                    ]
                  })(<Input
                    prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                    type="password"
                    onChange={e => this.setState({ password: e.target.value })}
                  />)
                }</Form.Item>
                <Form.Item label="Confirm Password">{
                  form.getFieldDecorator('confirm', {
                    rules: [
                      { required: true, message: 'Confirm Password is required' },
                      { validator: this.compareConfirmPasswordWithPassword }
                    ]
                  })
                  (<Input
                    prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                    type="password"
                    onChange={e => {
                      this.setState({ confirmPassword: e.target.value });
                      this.handleConfirmBlur(e)
                    }}
                  />)
                }</Form.Item>
                <Form.Item label="Email">{
                  form.getFieldDecorator('email', {
                    rules: [{
                      required: true,
                      message: 'Email is required'
                    }, {}]
                  })
                  (<Input
                    type="email"
                    onChange={e => this.setState({ email: e.target.value })}
                  />)
                }</Form.Item>
                <Form.Item label="First Name">{
                  form.getFieldDecorator('firstName', {
                    rules: [{
                      required: true,
                      message: 'First Name is required'
                    }]
                  })(<Input
                    prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                    onChange={e => this.setState({ firstName: e.target.value })}
                  />)
                }</Form.Item>
                <Form.Item label="Last Name">{
                  form.getFieldDecorator('lastName', {
                    rules: [{
                      required: true,
                      message: 'Last Name is required'
                    }]
                  })(<Input
                    prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                    onChange={e => this.setState({ lastName: e.target.value })}
                  />)
                }</Form.Item>
                <Form.Item label="Role">{
                  form.getFieldDecorator('role', {
                    rules: [{ required: true, message: 'Role must select' }],
                  })(
                    <Select
                      placeholder="Select Teacher or Student"
                      onChange={this.handleSelectRole}
                    >{
                      this.roles.map((role, idx) => <Option key={idx} value={role.toUpperCase()}>{role}</Option>)
                    }</Select>,
                  )
                }</Form.Item>
                {role === 'TEACHER' && (
                  <Form.Item label="Bank">{
                    form.getFieldDecorator('bank', {
                      rules: [{ required: true, message: 'Bank must select' }]
                    })(
                      <Select
                        placeholder="Select Bank for your income"
                        onChange={this.handleSelectBank}
                      >{
                        bankList.map((bank, idx) => (
                          <Option value={bank.name}>{`${bank.name.replace('_', ' ')} / ${bank.nameTh}`}</Option>
                        ))
                      }</Select>,
                    )
                  }</Form.Item>
                )}
                {role === 'TEACHER' && (
                  <Form.Item label="Account No.">{
                    form.getFieldDecorator('accountNo', {
                      rules: [
                        { required: true, message: 'Account No. must not be null' },
                        { validator: this.validateAccNo }
                      ],
                    })(<Input
                      prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                      onChange={e => this.setState({ accountNo: e.target.value })}
                    />)
                  }</Form.Item>
                )}
                {role === 'TEACHER' && (
                  <Form.Item label="Account Name">{
                    form.getFieldDecorator('accountName', {
                      rules: [{ required: true, message: 'Account Name must not be null' }],
                    })(<Input
                      prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                      onChange={e => this.setState({ accountName: e.target.value })}
                    />)
                  }</Form.Item>
                )}
              </Row>
              <Row type="flex" justify="center">
                <Col md={8} sm={12} xs={24}>
                  <Form.Item style={{ textAlign: 'center' }}>
                    <Button type="primary" htmlType="submit" className="register-form-button">
                      Register
                    </Button>
                  </Form.Item>
                </Col>
              </Row>
            </Form>
          </Col>

          <Col xxl={9} xl={8} lg={7} md={6} sm={5} xs={4} />
        </Row>
      </>
    );
  }
}

const signUpForm = Form.create()(SignUp);
export default withRouter(signUpForm);
