import 'antd/dist/antd.css'
import './App.css';

import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import Home from "./pages/home";
import Register from "./pages/sign-up";
import SignIn from "./pages/sign-in";

class App extends Component {

  render() {
    return (
      <Router>
        <Switch>
          <Route exact path="/sign-up" component={() => <Register />} />
          <Route exact path="/sign-in" component={SignIn} />
          <Route path="/" component={Home} />
          <Redirect to="/" />
        </Switch>
      </Router>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.user
  }
};

export default connect(mapStateToProps, null)(App);
