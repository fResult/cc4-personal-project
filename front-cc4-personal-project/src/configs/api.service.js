import axios from 'axios';
import { TOKEN } from './constants';
import { SIGN_OUT } from '../redux/constants/action-type';

import store from '../redux/store/store';

axios.defaults.baseURL = 'http://localhost:3333';

const UNPROTECTED_PATHS = [
  '/',
  'sign-in',
  'sign-up',
  // @TODO: Add courses path, enroll path and etc.
];

const isUnprotectedPath = url => {
  UNPROTECTED_PATHS.forEach(path => {
    if (url.includes(path)) {
      return true
    }
  });
  return false
};

axios.interceptors.request.use(async config => {
    if (isUnprotectedPath(config.url)) {
      return config;
    }

    let token = localStorage.getItem(TOKEN);

    config.headers['Authorization'] = `Bearer ${token}`;
    return config;
  }, async error => {
    throw error
  }
);

// Redirect to login page in case of 401 response
axios.interceptors.response.use(async config => {
    return config;
  }, async error => {
    if (error.request === undefined) throw error;

    let url = error.request.responseURL;
    if (error.request.status === 401 && isUnprotectedPath(url)) {
      throw error;
    }

    if (error.request.status === 401) {
      console.log('Session expire, redirect to login');

      localStorage.removeItem(TOKEN);
      store.dispatch({ type: SIGN_OUT })
    }
    throw error;
  }
);

export default axios;