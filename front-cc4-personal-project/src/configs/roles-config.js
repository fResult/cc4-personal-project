const components = {
  signIn: {
    component: 'SignIn',
    url: '/sign-in'
  },
  signUp: {
    component: 'SignUp',
    url: '/sign-up'
  },
  courses: {
    component: 'Courses',
    url: '/courses'
  }
};

export default {
  // role name as a key
  admin: {
    routes: [...Object.values(components)]
  },
  teacher: {
    routes: [
      components.courses,
    ]
  },
  student: {
    routes: [
      components.courses,
    ]
  },
  guest: {
    routes: [
      components.courses,
      components.signIn,
      components.signUp
    ]
  }
}