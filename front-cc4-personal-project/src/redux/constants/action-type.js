export const SIGN_IN = 'SIGN_IN';
export const SIGN_OUT = 'SIGN_OUT';
export const ADD_COURSE = 'ADD_COURSE';
export const FETCH_COURSES = 'FETCH_COURSES';
export const SEARCH_COURSES = 'SEARCH_COURSES';
export const SELECT_COURSE = 'SELECT_COURSE';
