import axios from 'axios';
import { TOKEN } from '../../configs/constants';
import { FETCH_COURSES, SELECT_COURSE, SIGN_IN, SIGN_OUT } from '../constants/action-type';

// @TODO: Moved this function
function fetchSignIn(token) {
  // @TODO
}

export function signIn(user, token) {
  fetchSignIn(token);
  return {
    type: SIGN_IN,
    ...user
  }
}

// @TODO: Moved this function
function fetchSignOut() {
  localStorage.removeItem(TOKEN);
}

export function signOut() {
  fetchSignOut();
  return {
    type: SIGN_OUT
  }
}

export function selectCourse(courseId) {
  return async function (dispatch) {
    return dispatch({
      type: SELECT_COURSE,
      course: (await axios.get(`/courses/${courseId}`)).data
    })
  }
}

export function searchCourses(text) {
  console.log('searchCourses', searchCourses)
  return async function (dispatch) {
    return dispatch({
      type: FETCH_COURSES,
      courses: (await axios.get(`/teachings?courseName=${text}&firstName=${text}&lastName=${text}`)).data
    });
  }
}

export function fetchCourses() {
  return async function (dispatch, getState) {
    if (getState().courses.length === 0) {
      return dispatch({ type: FETCH_COURSES, courses: (await axios.get('/courses')).data });
    }
  }
}