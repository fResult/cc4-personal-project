import { SELECT_COURSE, ADD_COURSE, FETCH_COURSES, SEARCH_COURSES } from "../constants/action-type";

function coursesReducer(coursesState = [], action) {
  switch (action.type) {
    case FETCH_COURSES:
      return action.courses;
    case SEARCH_COURSES:
      return action.courses;
    case SELECT_COURSE:
      return action.course;
    case ADD_COURSE:
      return coursesState;
    default:
      return coursesState;
  }
}

export default coursesReducer;
