import { SIGN_IN, SIGN_OUT } from "../constants/action-type";
import jwtDecode from 'jwt-decode';

const initialState = () => {
  const token = localStorage.getItem('ACCESS_TOKEN');
  if (token) {
    return jwtDecode(token);
  } else {
    return {
      role: 'guest'
    }
  }
};

function userReducer(userState = initialState(), action) {
  switch (action.type) {
    case SIGN_IN:
      return {
        ...userState,
        ...action
      };
    case SIGN_OUT:
      return { role: 'guest' };
    default:
      return userState;
  }
}

export default userReducer;