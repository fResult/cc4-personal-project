import React, { Component } from 'react';
import moment from 'moment';
import { Button, Col, Rate, Row, Typography, notification, Icon } from 'antd';
import axios from '../../configs/api.service';
import { connect } from "react-redux";

const { Title, Text, Paragraph } = Typography;

class Course extends Component {

  state = {
    course: {}
  };

  componentDidMount = () => {
    this.setState({ course: JSON.parse(localStorage.getItem('COURSE')) });
    window.scrollTo(0, 0);
  };

  openEnrollSuccessNotification = () => {
    const key = Date.now();
    notification.open({
      message: 'Enroll successfully',
      description: 'This course is enrolled',
      key, onClose: '', placement: 'topRight',
      icon: <Icon type="check-circle" theme="filled" style={{ fontSize: 28, color: '#0B8235' }} />
    });
  };

  openEnrollErrorNotification = (errorMessage) => {
    const key = Date.now();
    notification.open({
      message: 'Enroll failed',
      description: errorMessage,
      key, onClose: '', placement: 'topRight',
      icon: <Icon type="close-circle" theme="filled" style={{ fontSize: 28, color: 'crimson' }} />
    });
  };

  handleEnroll = async (courseId) => {
    try {
      await axios.post(`/enrollments/users/${this.props.user.id}/courses/${courseId}`);
      this.openEnrollSuccessNotification();
    } catch (ex) {
      this.openEnrollErrorNotification(ex.response.data.errorMessage);
    }
  };

  render() {
    const { course } = this.state;
    return (
      <>
        <Col span={2} />
        <Col span={20}>
          <Row>

            <div>
              <Title level={1}>{course.name}</Title>
              <Text style={{ fontSize: 24 }}>999 Reviews</Text> <br />
              <Text style={{ fontSize: 24 }}>999 people has already enrolled</Text> <br />
              <Text style={{ fontSize: 24 }}>Price: {course.price} Bath</Text> <br />
              <Button size="large" type="primary" onClick={() => this.handleEnroll(course.id)}>Let's Enroll</Button>
            </div>

            <br /> <br />

            <div>
              <Col span={12}>
                <Title level={1}>About This Course</Title>
                <Text style={{ fontSize: 24 }}>{course.desc_long}</Text>
              </Col>
              <Col span={12}>
                <Text style={{ fontSize: 24 }}>Published on {moment(course.createdAt).format('DD MMM, YYYY')}</Text>
                <br />
                <Text style={{ fontSize: 24 }}>999 Students</Text> <br />
                <Text style={{ fontSize: 24 }}>3 hours on demand</Text> <br />
                <Text style={{ fontSize: 24 }}>Has certificated</Text> <br />
              </Col>
            </div>

            <div>
              <Title level={1}>Reviews</Title>
              <div>
                <Title level={4}>ธีรนันท์ สหัสภาษส์</Title>
                <Rate disabled allowHalf defaultValue={4.5} />
                <Paragraph style={{ textIndent: 40 }}>
                  เดิมทีผมเป็นคนที่ไม่มีพื้นฐานการเขียนโปรแกรมมาก่อน แต่ผู้สอนสามารถสอนให้ผมเข้าใจ เขียนได้
                  สร้างโปรแกรมเป็นครับ ขอบคุณที่พี่ XXX ทำคอร์สดีๆ แบบนี้ออกมาให้ครับ เอาไปเลย 1 Star!
                </Paragraph>
              </div>
            </div>


            <div>
              <Title level={1}>Outline</Title>
              {course.chapters && course.chapters.map((chapter, idx) => (
                <div key={chapter.id}>
                  <Title level={3}>Chapter {idx + 1}: {chapter.name}</Title>
                  <Paragraph style={{ textIndent: 40 }}>
                    DESCRIPTION DESCRIPTION DESCRIPTION DESCRIPTION DESCRIPTION DESCRIPTION DESCRIPTION DESCRIPTION
                    DESCRIPTION DESCRIPTION DESCRIPTION DESCRIPTION DESCRIPTION DESCRIPTION DESCRIPTION DESCRIPTION
                    DESCRIPTION DESCRIPTION DESCRIPTION DESCRIPTION DESCRIPTION DESCRIPTION DESCRIPTION DESCRIPTION
                  </Paragraph>
                </div>
              ))}
            </div>

            <div>
              <Title level={1}>Teachers Information</Title>
              {course.users && course.users.map(teacher => (
                <div key={teacher.id}>
                  <Title level={3}>{`${teacher.first_name} ${teacher.last_name}`}</Title>
                  <Row>
                    <Col span={18}>
                      <Paragraph style={{ textIndent: 40 }}>
                        {teacher.about_teacher}
                      </Paragraph>
                    </Col>
                    <Col span={6}>
                      <img style={{ width: '100%' }} alt=""
                           src={teacher.img_url}
                      />
                    </Col>
                  </Row>
                </div>
              ))}
            </div>
          </Row>
        </Col>
        <Col span={2} />
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    user: state.user
  }
};

export default connect(mapStateToProps, null)(Course);
