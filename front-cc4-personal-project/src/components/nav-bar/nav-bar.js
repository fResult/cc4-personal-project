import React, { Component } from 'react';
import { Input, Row, Col, Button, Menu, Dropdown, notification, Icon } from 'antd';
import { Link, withRouter } from "react-router-dom";
import { connect } from "react-redux";

import styles from './nav-bar.module.css';
import { signOut, searchCourses } from '../../redux/actions/action-creators';

const WAIT_INTERVAL = 700;
const ENTER_KEY = 13;

class NavBar extends Component {

  state = {
    courseSearch: undefined
  };

  UNSAFE_componentWillMount = () => {
    this.timer = null;
  };

  componentDidUpdate = async () => {
  };

  handleChangeSearch = (e) => {
    clearTimeout(this.timer);
    this.timer = setTimeout(this.triggerChange.bind(null, e.target.value), WAIT_INTERVAL);
  };

  handleKeyUp = (e) => {
    if (e.keyCode === ENTER_KEY) {
      this.triggerChange.bind(null, e.target.value);
    }
  };

  triggerChange = async (value) => {
    this.setState({ courseSearch: value });
    const courses = await this.props.searchCourses(value);
    console.log('courses', courses);
  };

  openLoggedOutNotification = () => {
    const key = Date.now();
    notification.open({
      message: 'Log Out Successfully',
      description: `${this.props.user.first_name} is logged out`,
      key, onClose: '', placement: 'topRight',
      icon: <Icon type="logout" style={{ fontSize: 28, color: 'crimson' }} />
    });
  };

  handleSignOut = () => {
    const { signOut } = this.props;

    this.openLoggedOutNotification();
    localStorage.removeItem('ACCESS_TOKEN');
    signOut();
  };

  userMenu = (
    <Menu className={styles.Menu + " ant-dropdown-link"} style={{ zIndex: 9999, position: 'fixed', top: 50 }}>
      {this.props.user.role === 'STUDENT' && (
        <Menu.Item key="my-courses">
          <Link to="/my-courses">My Courses</Link>
        </Menu.Item>
      )}
      {this.props.user.role === 'TEACHER' && (
        <Menu.Item key="my-own-courses">
          <Link to="/my-own-courses">My Own Courses</Link>
        </Menu.Item>
      )}
      <Menu.Item key="change-password">
        <Link to="/change-password">Change Password</Link>
      </Menu.Item>
      <Menu.Item key="2nd">2nd menu item</Menu.Item>
      <Menu.Divider />
      <Menu.Item key="logout">
        <Link to="/" onClick={this.handleSignOut}>Log out</Link>
      </Menu.Item>
    </Menu>
  );

  render() {
    const { user } = this.props;
    return (
      <>
        <Row>
          <Col span={6}>
            <div style={{ margin: '10px 0' }}>
              <Link to="/">
                <Icon type="home" style={{ fontSize: 48, color: '#FFFFFF' }} />
              </Link>
            </div>
          </Col>

          <Col span={12}>
            <Input.Search className={styles.Search} size="large"
                          placeholder={"Search course or teacher"}
                          onChange={this.handleChangeSearch}
                          onKeyUp={this.handleKeyUp}
                          style={{ fontSize: 24 }}
            />
          </Col>

          <Col span={6}>
            <Row type="flex" justify="end">
              {user.role === 'guest' && (
                <Link className={`${styles.Link} ant-btn ant-btn-primary`} to="/sign-up">
                  <Button className={styles.Button} size="large">Register</Button>
                </Link>
              )}
              {user.role === 'guest' && (
                <Link className={`${styles.Link} ant-btn ant-btn-primary`} to="/sign-in">
                  <Button className={styles.Button} size="large">Login</Button>
                </Link>
              )}
              {user.role !== 'guest' && (
                <Link className={`${styles.Link} ant-btn ant-btn-primary`} to="/sign-out">
                  <Dropdown overlay={this.userMenu} trigger={['click']} placement="bottomRight">
                    <Button className={`${styles.Button} ant-dropdown-link `}>
                      {(user.first_name[0] + user.last_name[0]).toUpperCase()} <Icon type="down" />
                    </Button>
                  </Dropdown>
                </Link>
              )}
            </Row>
          </Col>
        </Row>
      </>
    );
  }
}

const mapDispatchToProps = {
  searchCourses: searchCourses,
  signOut: signOut
};

const mapStateToProps = (state) => {
  return {
    user: state.user
  }
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(NavBar))
