import React, { Component } from 'react';
import { Button, Card, Col, Row, Typography } from "antd";

import styles from './courses.module.css';
import { Link } from "react-router-dom";

const { Meta } = Card;
const { Title, Text, Paragraph } = Typography;

class Courses extends Component {

  render() {
    const { courses } = this.props;
    return (
      <>
        <Row>
          <Col lg={2} md={4} sm={2} />
          <Col lg={20} md={16} sm={20}>
            <Row gutter={[30, 30]} type="flex" justify="center">{
              courses && courses.map(course => (
                <Link key={course.id} to={`/courses/${course.id}`} onClick={() => this.props.onSelectCourse(course.id)}>
                  <Card hoverable
                        style={{ max: '100%', maxWidth: 400, margin: '15px 15px' }}
                        cover={<img alt="example" src={course.image_url} />}
                  >
                    <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                      <Text code>{course.type}</Text>
                      <Text className={styles.Price}>{course.price} Baht</Text>
                    </div>
                    <Meta title={<Title className={styles.CourseName} level={4}>{course.name}</Title>}
                          description={<Paragraph>{course.desc_short}</Paragraph>} />
                    <div style={{ textAlign: 'right' }}>
                      <Button>See more...</Button>
                    </div>
                  </Card>
                </Link>
              ))
            }</Row>
          </Col>
        </Row>
        <Col lg={2} md={4} sm={2} />
      </>
    );
  }
}

export default Courses;
