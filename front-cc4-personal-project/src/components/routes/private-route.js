import React, { Component } from 'react';
import rolesConfig from '../../configs/roles-config';
import { Route, withRouter, Redirect } from 'react-router-dom';

class PrivateRoute extends Component {
  state = {
    allowedRoutes: []
  };

  componentDidMount() {
    const { role } = this.props;
    if (role) {
      this.setState({
        allowedRoutes: rolesConfig[role].routes
      })
    } else {
      this.props.history.push('/sign-in')
    }
  }

  render() {
    return (
      <>
        {this.state.allowedRoutes.map(route => (
          <Route
            exact path={route.url}
            component={shallowEqual[route.component]}
            key={route.url}
          />
        ))}
        {this.props.role === 'guest' ? <Redirect to='/' /> : null}
      </>
    );
  }
}

export default withRouter(PrivateRoute);