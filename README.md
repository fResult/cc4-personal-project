# Code Camp 4 - Personal Project - Online Courses  Web application

## Prepare to run this web application

In the project directory, you must run first: 

### `cd front-cc4-personal-project && yarn && cd ../back-cc4-personal-project && yarn && cd ..`  


## Available Scripts


In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.  
Open [http://localhost:8080](http://localhost:8080) to view it in the browser. (Client side)  
And [http://localhost:3333](http://localhost:3333) will be your server side.

Run only Backend:  
### `yarn start:backend`
<br/>

Run only Frontend:
### `yarn start:frontend`

## Using Database generator
### Edit `db.sequelize.sync({force: false})` in index.js (If it is false)  to be `db.sequelize.sync({force: true})`
### Create database **cc4_athena_personal_project** in your MySql `CREATE SCHEMA cc4_athena_personal_project`
### In the project directory, run `yarn sequelize db:seed:all`

## UI Designed
[UI Designed](https://www.figma.com/file/vqV1QuCHmlIWHFP27FVJ5A)  
[Prototype Designed](https://www.figma.com/proto/vqV1QuCHmlIWHFP27FVJ5A)
[Database Design](https://gitlab.com/fResult/cc4-personal-project/tree/master/design) <== Open by draw.io
